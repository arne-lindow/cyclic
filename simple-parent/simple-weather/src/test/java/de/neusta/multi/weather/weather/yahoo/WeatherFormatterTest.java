package de.neusta.multi.weather.weather.yahoo;

import java.io.InputStream;

import de.neusta.multi.weather.weather.Weather;
import de.neusta.multi.weather.weather.WeatherFormatter;
import de.neusta.multi.weather.weather.YahooParser;
import org.apache.commons.io.IOUtils;

import junit.framework.TestCase;

public class WeatherFormatterTest extends TestCase {

	public WeatherFormatterTest(String name) {
		super(name);
	}
	
	public void testFormat() throws Exception {
		InputStream nyData = 
			getClass().getClassLoader().getResourceAsStream("ny-weather.xml");
		Weather weather = new YahooParser().parse( nyData );
		String formattedResult = new WeatherFormatter().format( weather );
		InputStream expected = 
			getClass().getClassLoader().getResourceAsStream("format-expected.dat");
		assertEquals( IOUtils.toString( expected ), formattedResult );
	}
}
