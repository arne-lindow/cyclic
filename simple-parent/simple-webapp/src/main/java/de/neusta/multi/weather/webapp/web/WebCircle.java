package de.neusta.multi.weather.webapp.web;

import de.neusta.multi.weather.webapp.dao.DaoCircle;
import de.neusta.multi.weather.webapp.service.ServiceCircle;

public class WebCircle {

    public DaoCircle get() {
        return new DaoCircle();
    }

    public ServiceCircle getFalse() {
        return new ServiceCircle();
    }

}
