package de.neusta.multi.weather.webapp.service;

import de.neusta.multi.weather.webapp.dao.DaoCircle;
import de.neusta.multi.weather.webapp.web.WebCircle;

public class ServiceCircle {

    public WebCircle get() {
        return new WebCircle();
    }

    public DaoCircle getRight() {
        return new DaoCircle();
    }

}
